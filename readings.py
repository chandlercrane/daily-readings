from datetime import datetime
from pytz import timezone
import time
import requests
import re
from bs4 import BeautifulSoup
import tweepy
from credentials import *

# get eastern time zone date
est = timezone('EST')
today = datetime.now(est)
month = today.strftime("%m")
day = today.strftime("%d")
year = today.strftime("%y")

# get USCCB link for today's daily readings
url = "https://bible.usccb.org/bible/readings/" + month + day + year + ".cfm"
page = requests.get(url)

# parse the page with BS
soup = BeautifulSoup(page.text, 'html.parser')

readings = soup.find_all('div', class_='p-wrap col-lg-10 offset-lg-1 col-xl-8 offset-xl-2 col-xxl-6 offset-xxl-3')

# Get day name (e.g Saturday of the Fifth Week in Ordinary Time)
day_name = readings[0].find_all('h2')[0].text.strip()

daily_readings = []
section = {}
readings = readings[1:-3]

for reading in readings:
  header = reading.find_all('div', class_='content-header')

  section['name'] = header[0].find_all('h3', class_='name')[0].text.strip()
  section['address'] = header[0].find_all('div', class_='address')[0].text.strip()

  body = reading.find_all('div', class_='content-body')
  paragraphs = []
  for paragraph in body:
    paragraphs.append(paragraph.text)
  lines = re.split('\n|[\xa0* ]*\xa0+', ''.join(paragraphs))
  section['text'] = ' '.join([i for i in lines if i])
  daily_readings.append(section.copy())

def split_into_tweets(section):
  size_limit = 280
  tweet = ""
  tweet += day_name + '\n' + section['name'] + '\n' + section['address'] + '\n\n'

  words = section['text'].split(' ')

  tweets = []
  count = 0
  while count < len(words):
    while count < len(words) and len(tweet) + len(words[count]) < size_limit:
      tweet += words[count] + ' '
      count += 1
    tweets.append(tweet)
    tweet = ""
  section['tweets'] = tweets
  return section

# authentication

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

# Create API Object
api = tweepy.API(auth)

# send tweet

for dr in daily_readings:
  section = split_into_tweets(dr)
  
  status_id = -1
  for tweet in section['tweets']:
    if status_id < 0:
      status = api.update_status(tweet)
    else:
      status = api.update_status(tweet, in_reply_to_status_id=status_id)
    status_id = status.id
    time.sleep(2)