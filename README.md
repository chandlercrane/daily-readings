# Daily Readings Twitter Bot

This project is a Twitter Bot that posts the daily readings each day at 7am ET to the Twitter Account [@lentenpromise](https://www.twitter.com/lentenpromise)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing Dependencies

What things you need to install to create your own clone Twitter bot:

```
pip install -r requirements.txt 
```

Next, you will need to create a `credentials.py` file to store the following access tokens:
```
CONSUMER_KEY = "<CONSUMER_KEY>"
CONSUMER_SECRET = "<CONSUMER_SECRET>"
BEARER_TOKEN = "<BEARER_TOKEN>"
ACCESS_TOKEN = "<ACCESS_TOKEN>"
ACCESS_TOKEN_SECRET = "<ACCESS_TOKEN_SECRET>"
```

Please also note you will have to have created a Twitter project and app through your Twitter devloper account at [Twitter's Developer Site](https://developer.twitter.com/en)


### Run the Code

To execute the file and tweet out the daily readings, run the following command:
```
python3 readings.py
```

## Built With

* [Tweepy](https://www.tweepy.org/) - Python library for accessing Twitter API
* [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) - HTML Parser

## Authors

* **Chandler Crane** - [About Me](https://www.linkedin.com/in/chandlercrane/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
